package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.model.dto.GoodsExample;
import com.kgc.cn.common.model.dto.Promotion;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

public interface GoodsMapper {
    long countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(String goodId);

    int insert(Goods record);

    int insertSelective(Goods record);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKey(String goodId);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    //商品列表要按类型来展示商品列表（从后台中获取商品列表）
    List<Goods> ProductList(@Param("typeId") int typeId, @Param("start") int start, @Param("size") int size) throws Exception;

    //查询该类型商品总数量
    int queryCount(@Param("typeId") int typeId);

    //商品详情（要体现 名字 原价 图片 描述）
    Goods ProductsDetails(String goodId);

    //商品：折扣
    Promotion DiscountParam(String goodId) throws Exception;

    // 根据商品id查询商品折扣
    Promotion selectDiscountByGoodId(String goodId);

    // 更新库存
    int UpdateNum(@Param("goodNum") int goodNum,@Param("goodId") String goodId);

}