package com.kgc.cn.provider.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.model.dto.Promotion;
import com.kgc.cn.common.model.dto.Shopcar;
import com.kgc.cn.common.model.dto.ShopcarExample;
import com.kgc.cn.common.service.ShopcarCommonService;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.ShopcarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by boot on 2019/12/24
 */
@Service
public class ShopcarProviderServiceImpl implements ShopcarCommonService {

    @Autowired
    private ShopcarMapper shopcarMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 根据手机添加购物车
     *
     * @param shopcar
     * @return
     */
    @Override
    public int insertByPhone(Shopcar shopcar) {
        return shopcarMapper.insert(shopcar);
    }

    /**
     * 根据手机号和商品id删除购物车信息，未输入商品id则清空购物车
     *
     * @param phone
     * @param goodId
     * @return
     */
    @Override
    public int deleteByPhoneAndGoodId(String goodId, String phone) {
        return shopcarMapper.deleteByPhoneAndGoodId(goodId, phone);
    }

    /**
     * 根据商品id查询商品信息
     *
     * @param goodId
     * @return
     */
    @Override
    public Goods queryGoodByGoodId(String goodId) {
        return goodsMapper.selectByPrimaryKey(goodId);
    }

    /**
     * 通过goodId查询折扣信息
     *
     * @param goodId
     * @return
     */
    @Override
    public Promotion queryPromotionByGoodId(String goodId) {
        return goodsMapper.selectDiscountByGoodId(goodId);
    }

    /**
     * 根据手机号查询购物车
     *
     * @param phone
     * @return
     */
    @Override
    public List<Shopcar> queryShopcarByPhone(String phone,int current,int size) {
        int start = (current-1)*size;
        return shopcarMapper.queryShopcarByPhone(phone,start,size);
    }

    /**
     * 根据手机号查询购物车数量
     *
     * @param phone
     * @return
     */
    @Override
    public int queryNumByPhone(String phone){
        return shopcarMapper.queryNumByPhone(phone);
    }

    /**
     * 查询改手机号下，对应商品id的购物车对象
     *
     * @param phone
     * @param goodId
     * @return
     */
    @Override
    public Shopcar queryShopCarByGoodId(String phone, String goodId) {
        ShopcarExample shopcarExample = new ShopcarExample();
        shopcarExample.createCriteria()
                .andPhoneEqualTo(phone)
                .andGoodIdEqualTo(goodId);
        List<Shopcar> shopcarList = shopcarMapper.selectByExample(shopcarExample);
        if (!CollectionUtils.isEmpty(shopcarList)) {
            return shopcarList.get(0);
        }
        return null;
    }

    /**
     * 更新手机号下，该商品id对应的商品数量
     *
     * @param shopcar
     * @return
     */
    @Override
    public int updateShopCarByPhone(Shopcar shopcar) {
        return shopcarMapper.updateShopCarByPhone(shopcar);
    }
}
