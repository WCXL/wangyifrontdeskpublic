package com.kgc.cn.provider.utils;

import com.kgc.cn.common.utils.Date.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
@Component
public class OrderUtil {

    public String generateOrderId(String phone) throws Exception {
        Date date = new Date();
        return DateUtils.formatYYYYMMDDHHSS(date) + phone.substring(5, 11);
    }
}
