package com.kgc.cn.provider.utils.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

/**
 * @auther zhouxinyu
 * @data 2019/12/13
 */
public class EmployeeExcelModel extends BaseRowModel {
    @ExcelProperty(index = 0)
    private String empName;
    @ExcelProperty(index = 1)
    private Integer empSex;
    @ExcelProperty(index = 2)
    private Integer empAge;
    @ExcelProperty(index = 3)
    private String empEmail;
    @ExcelProperty(index = 4)
    private String empPhone;
    @ExcelProperty(index = 5)
    private String empPassword;
    @ExcelProperty(index = 6)
    private String empAddress;
    @ExcelProperty(index = 7)
    private String empIdentityCard;
    @ExcelProperty(index = 8)
    private String empTimeStart;
    @ExcelProperty(index = 9)
    private Double empSalary;
    @ExcelProperty(index = 10)
    private Integer roleId;
    @ExcelProperty(index = 11)
    private Integer departId;

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getEmpSex() {
        return empSex;
    }

    public void setEmpSex(Integer empSex) {
        this.empSex = empSex;
    }

    public Integer getEmpAge() {
        return empAge;
    }

    public void setEmpAge(Integer empAge) {
        this.empAge = empAge;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpPassword() {
        return empPassword;
    }

    public void setEmpPassword(String empPassword) {
        this.empPassword = empPassword;
    }

    public String getEmpAddress() {
        return empAddress;
    }

    public void setEmpAddress(String empAddress) {
        this.empAddress = empAddress;
    }

    public String getEmpIdentityCard() {
        return empIdentityCard;
    }

    public void setEmpIdentityCard(String empIdentityCard) {
        this.empIdentityCard = empIdentityCard;
    }

    public String getEmpTimeStart() {
        return empTimeStart;
    }

    public void setEmpTimeStart(String empTimeStart) {
        this.empTimeStart = empTimeStart;
    }

    public Double getEmpSalary() {
        return empSalary;
    }

    public void setEmpSalary(Double empSalary) {
        this.empSalary = empSalary;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getDepartId() {
        return departId;
    }

    public void setDepartId(Integer departId) {
        this.departId = departId;
    }

    @Override
    public String toString() {
        return "EmployeeExcelModel{" +
                "empName='" + empName + '\'' +
                ", empSex=" + empSex +
                ", empAge=" + empAge +
                ", empEmail='" + empEmail + '\'' +
                ", empPhone='" + empPhone + '\'' +
                ", empPassword='" + empPassword + '\'' +
                ", empAddress='" + empAddress + '\'' +
                ", empIdentityCard='" + empIdentityCard + '\'' +
                ", empTimeStart='" + empTimeStart + '\'' +
                ", empSalary=" + empSalary +
                ", roleId=" + roleId +
                ", departId=" + departId +
                '}';
    }
}
