package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.common.model.dto.WeixinuserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WeixinuserMapper {
    long countByExample(WeixinuserExample example);

    int deleteByExample(WeixinuserExample example);

    int deleteByPrimaryKey(String openId);

    int insert(Weixinuser record);

    int insertSelective(Weixinuser record);

    List<Weixinuser> selectByExample(WeixinuserExample example);

    Weixinuser selectByPrimaryKey(String openId);

    int updateByExampleSelective(@Param("record") Weixinuser record, @Param("example") WeixinuserExample example);

    int updateByExample(@Param("record") Weixinuser record, @Param("example") WeixinuserExample example);

    int updateByPrimaryKeySelective(Weixinuser record);

    int updateByPrimaryKey(Weixinuser record);

    //通过openid更新手机号
    int updatePhoneByOpenid(@Param("openid") String openid, @Param("phone") String phone);
}