package com.kgc.cn.provider.utils.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

/**
 * @auther zhouxinyu
 * @data 2019/12/12
 */
public class GoodsExcelModel extends BaseRowModel {
    @ExcelProperty(index = 0)
    private String goodName;
    @ExcelProperty(index = 1)
    private int goodPrice;
    @ExcelProperty(index = 2)
    private int goodNum;
    @ExcelProperty(index = 3)
    private int typeId;
    @ExcelProperty(index = 4)
    private String pictureSource;
    @ExcelProperty(index = 5)
    private String goodContent;

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public int getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(int goodPrice) {
        this.goodPrice = goodPrice;
    }

    public int getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(int goodNum) {
        this.goodNum = goodNum;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getPictureSource() {
        return pictureSource;
    }

    public void setPictureSource(String pictureSource) {
        this.pictureSource = pictureSource;
    }

    public String getGoodContent() {
        return goodContent;
    }

    public void setGoodContent(String goodContent) {
        this.goodContent = goodContent;
    }

    @Override
    public String toString() {
        return "GoodsExcelModel{" +
                "goodName='" + goodName + '\'' +
                ", goodPrice='" + goodPrice + '\'' +
                ", goodNum='" + goodNum + '\'' +
                ", typeId='" + typeId + '\'' +
                ", pictureSource='" + pictureSource + '\'' +
                ", goodContent='" + goodContent + '\'' +
                '}';
    }
}
