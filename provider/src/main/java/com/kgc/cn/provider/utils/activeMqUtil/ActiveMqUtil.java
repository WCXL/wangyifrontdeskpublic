package com.kgc.cn.provider.utils.activeMqUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * @auther zhouxinyu
 * @data 2019/12/14
 */
@Component
public class ActiveMqUtil {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 发邮件消息中间件
     *
     * @param name
     * @param employee
     */
    /*public void sendEmailByActiveMq(String name, Employee employee) {
        Queue queue = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(queue, employee);

    }*/

}
