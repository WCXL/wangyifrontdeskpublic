package com.kgc.cn.provider.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.UserExample;
import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.common.service.WxUserService;
import com.kgc.cn.provider.mapper.UserMapper;
import com.kgc.cn.provider.mapper.WeixinuserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by Ding on 2019/12/23.
 */
@Service
public class WxUserServiceImpl implements WxUserService {
    @Autowired
    private WeixinuserMapper wxMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 通过openid查询微信用户
     *
     * @param openId
     * @return
     */
    @Override
    public Weixinuser queryUserByOpenId(String openId) {
        return wxMapper.selectByPrimaryKey(openId);
    }

    /**
     * 新增一个微信用户对象
     *
     * @param weixinuser
     * @return
     */
    @Override
    @Transactional
    public int insertWeiXinUser(Weixinuser weixinuser) {
        return wxMapper.insert(weixinuser);
    }

    /**
     * 通过手机号查询普通用户
     *
     * @param phone
     * @return
     */
    @Override
    public User queryUserByPhone(String phone) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andPhoneEqualTo(phone);
        List<User> userList = userMapper.selectByExample(userExample);
        if (!CollectionUtils.isEmpty(userList)) {
            return userList.get(0);
        }
        return null;
    }

    /**
     * 通过openid更新手机号
     *
     * @param openid
     * @param phone
     * @return
     */
    @Override
    public int updateWxUserByOpenid(String openid, String phone) {
        return wxMapper.updatePhoneByOpenid(openid, phone);
    }

    @Override
    public int addUser(User user) {
        return userMapper.insert(user);
    }
}
