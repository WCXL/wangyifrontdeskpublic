package com.kgc.cn.consumer.service;

import com.kgc.cn.consumer.paramModel.GoodsParam;
import com.kgc.cn.consumer.paramModel.GoodsParamDetails;
import com.kgc.cn.consumer.paramModel.PageBeanParam;

public interface ProductsConsumerService {

    //商品列表要按类型来展示商品列表（从后台中获取商品列表）
    PageBeanParam<GoodsParam> ProductList(int typeId, int current, int size) throws Exception;

    //商品详情（要体现 名字 原价 实付价格 图片 描述）
    GoodsParamDetails ProductsDetails(String goodId) throws Exception;

    // 返回商品库存
    int ProductsNum(String goodId);
}
