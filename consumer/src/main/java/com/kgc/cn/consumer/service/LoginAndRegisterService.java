package com.kgc.cn.consumer.service;

import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;

/**
 * Created by Ding on 2019/12/23.
 */
public interface LoginAndRegisterService {
    //验证码登录
    String login(String phone, String code, HttpServletRequest request) throws Exception;

    //微信登录回调
    String callBack(String code);

    //获取验证码
    String getCode(String phone);

    //根据openid查询微信用户对象
    Weixinuser queryUserByOpenId(String openid);

    //新增一个微信用户对象
    int insertWeiXinUser(Weixinuser weixinuser);

    //通过手机号查询普通用户对象
    User queryUserByPhone(String phone);

    //绑定手机号
    String addPhone(Object o, String phone, String code, HttpServletRequest request);

}
