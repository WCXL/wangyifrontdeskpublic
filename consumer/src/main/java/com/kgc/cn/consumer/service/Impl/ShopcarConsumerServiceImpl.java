package com.kgc.cn.consumer.service.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.dto.*;
import com.kgc.cn.common.model.paging.PageBean;
import com.kgc.cn.common.service.ShopcarCommonService;
import com.kgc.cn.common.utils.Date.DateUtils;
import com.kgc.cn.consumer.paramModel.GoodsParamDetails;
import com.kgc.cn.consumer.paramModel.PageBeanParam;
import com.kgc.cn.consumer.paramModel.ShopcarInfoParam;
import com.kgc.cn.consumer.service.LoginAndRegisterService;
import com.kgc.cn.consumer.service.ProductsConsumerService;
import com.kgc.cn.consumer.service.ShopcarConsumerService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by boot on 2019/12/24
 */
@Service
public class ShopcarConsumerServiceImpl implements ShopcarConsumerService {

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private LoginAndRegisterService loginAndRegisterService;
    @Reference
    private ShopcarCommonService shopcarCommonService;
    @Autowired
    private ProductsConsumerService productsConsumerService;

    /**
     * 未登录添加购物车
     *
     * @param goodId
     * @param shopNum
     * @return
     */
    private String noLoginAdd(String goodId, int shopNum) throws UnknownHostException {
        String ip = ipAddress();
        Goods goods = shopcarCommonService.queryGoodByGoodId(goodId);
        // 判断库存
        if (shopNum <= 0 || shopNum > goods.getGoodNum()) {
            return "201";
        }
        // 判断是否有键值对
        if (redisUtils.hHasKey(ip, goodId)) {
            redisUtils.hincr(ip, goodId, shopNum);
            return "0";
        } else {
            redisUtils.hset(ip, goodId, shopNum, 60 * 60 * 24);
            return "0";
        }
    }

    /**
     * 未登录删除购物车某个商品
     *
     * @param goodId
     */
    private void noLoginDel(String ip, String goodId) {
        if (redisUtils.hHasKey(ip, goodId)) {
            redisUtils.hdel(ip, goodId);
        }
    }

    /**
     * 未登录清空购物车
     *
     * @param ip
     * @throws UnknownHostException
     */
    private void noLoginDelAll(String ip) {
        redisUtils.del(ip);
    }

    /**
     * 未登录显示购物车列表
     *
     * @return
     */
    private List<ShopcarInfoParam> noLoginShow(String ip) {
        List<ShopcarInfoParam> shopcarInfoParamList = Lists.newArrayList();

        Map<String, Integer> shopcarMap = (Map) redisUtils.hmget(ip);
        shopcarMap.forEach((k, v) -> {
            try {
                GoodsParamDetails goodsParamDetails = productsConsumerService.ProductsDetails(k);
                ShopcarInfoParam shopcarInfoParam = ShopcarInfoParam.builder()
                        .shopNum(v)
                        .discountedPrice(goodsParamDetails.getSellMoney())
                        .build();
                BeanUtils.copyProperties(goodsParamDetails, shopcarInfoParam);
                shopcarInfoParam.setTotalPrice(shopcarInfoParam.getDiscountedPrice() * v);
                shopcarInfoParamList.add(shopcarInfoParam);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return shopcarInfoParamList;
    }

    /**
     * 添加购物车
     *
     * @param goodId
     * @param shopNum
     * @return
     */
    @Override
    public String addShopcar(String goodId, int shopNum, HttpServletRequest request) throws UnknownHostException {
        Goods goods = shopcarCommonService.queryGoodByGoodId(goodId);
        if (shopNum <= 0 || shopNum > goods.getGoodNum()) {
            return "201";
        }
        // 获取token并判断
        String token = request.getHeader("token");
        if (!StringUtils.isEmpty(token)) {
            String objectStr = redisUtils.get(token).toString();
            if (objectStr.contains("openid")) {
                JSONObject jsonObject = JSONObject.parseObject(objectStr);
                // 查询微信用户
                Weixinuser weixinuser = loginAndRegisterService.queryUserByOpenId(jsonObject.getString("openId"));
                // 判断微信用户有没有绑定手机号
                if (null != weixinuser.getPhone()) {
                    return mysqlJudge(weixinuser.getPhone(), goodId, shopNum);
                }
                return "200";
            } else {
                // 普通用户登录
                User user = JSONObject.parseObject(objectStr, User.class);
                return mysqlJudge(user.getPhone(), goodId, shopNum);
            }
        } else {
            return noLoginAdd(goodId, shopNum);
        }
    }

    /**
     * 展示购物车
     *
     * @param request
     * @return
     */
    @Override
    public PageBeanParam<ShopcarInfoParam> showShopcar(int current, int size, HttpServletRequest request) throws Exception {
        //定义分页实体类,设置分页数据
        PageBeanParam<ShopcarInfoParam> pageBeanParam = PageBeanParam.<ShopcarInfoParam>builder().build();
        pageBeanParam.setCurrent(current);
        pageBeanParam.setSize(size);
        String ip = ipAddress();
        // 获取token并判断
        String token = request.getHeader("token");
        if (!StringUtils.isEmpty(token)) {
            String objStr = redisUtils.get(token).toString();
            if (objStr.contains("openid")) {
                JSONObject jsonObject = JSONObject.parseObject(objStr);
                // 获取微信用户信息
                Weixinuser weixinuser = loginAndRegisterService.queryUserByOpenId(jsonObject.getString("openId"));
                if (null != weixinuser.getPhone()) {
                    // 微信用户绑定了手机号，获取购物车信息
                    List<Shopcar> shopcarList = shopcarCommonService.queryShopcarByPhone(weixinuser.getPhone(),current,size);
                    // 判断购物车是否为空
                    if (CollectionUtils.isEmpty(shopcarList)) {
                        return pageBeanParam;
                    } else {
                        pageBeanParam.setData(ergodic(shopcarList));
                        pageBeanParam.setCount(shopcarCommonService.queryNumByPhone(weixinuser.getPhone()));
                        return pageBeanParam;
                    }
                }
                return pageBeanParam;
            } else {
                // 普通用户登录
                User user = JSONObject.parseObject(objStr, User.class);
                List<Shopcar> userShopcarList = shopcarCommonService.queryShopcarByPhone(user.getPhone(),current,size);
                if (CollectionUtils.isEmpty(userShopcarList)) {
                    return pageBeanParam;
                } else {
                    pageBeanParam.setData(ergodic(userShopcarList));
                    pageBeanParam.setCount(shopcarCommonService.queryNumByPhone(user.getPhone()));
                    return pageBeanParam;
                }
            }
        } else {
            if (redisUtils.hasKey(ip)) {
                pageBeanParam.setData(noLoginShow(ip));
                pageBeanParam.setCount(redisUtils.hmget(ip).size());
                return pageBeanParam;
            }
            return pageBeanParam;
        }
    }

    /**
     * 删除购物车某件商品或者清空购物车
     *
     * @param goodId
     * @param request
     * @return
     */
    @Override
    public String deleteShopcar(String goodId, HttpServletRequest request) throws Exception {
        String ip = ipAddress();
        // 获取token并判断
        String token = request.getHeader("token");
        if (!StringUtils.isEmpty(token)) {
            // 不为空获取信息
            String objStr = redisUtils.get(token).toString();
            if (objStr.contains("openid")) {
                JSONObject jsonObject = JSONObject.parseObject(objStr);
                // 获取微信用户信息
                Weixinuser weixinuser = loginAndRegisterService.queryUserByOpenId(jsonObject.getString("openId"));
                if (null != weixinuser.getPhone()) {
                    // 微信用户绑定了手机号
                    shopcarCommonService.deleteByPhoneAndGoodId(weixinuser.getPhone(), goodId);
                    return "0";
                } else {
                    // 未绑定手机号返回错误
                    return "200";
                }
            } else {
                // 普通用户登录
                User user = JSONObject.parseObject(objStr, User.class);
                shopcarCommonService.deleteByPhoneAndGoodId(goodId, user.getPhone());
                return "0";
            }

        } else {
            // 判断商品id是否为空
            if (!StringUtils.isEmpty(goodId)) {
                noLoginDel(ip, goodId);
                return "0";
            } else {
                noLoginDelAll(ip);
                return "0";
            }
        }
    }

    /**
     * 将未登录状态下的购物车，转移到登录账户下
     *
     * @param phone
     * @return
     */
    @Override
    @Transactional
    public boolean transferShopCar(String phone) throws UnknownHostException {
        Map<String, Integer> shopMap = (Map) redisUtils.hmget(ipAddress());
        shopMap.forEach((k, v) -> {
            //查询购物车里有没有该商品
            Shopcar shopcars = shopcarCommonService.queryShopCarByGoodId(phone, k);
            if (null == shopcars) {
                Shopcar shopcar = Shopcar.builder()
                        .phone(phone)
                        .goodId(k)
                        .shopNum(v)
                        .build();
                shopcarCommonService.insertByPhone(shopcar);
            } else {
                Shopcar shopcar = Shopcar.builder()
                        .phone(phone)
                        .goodId(k)
                        .shopNum(shopcars.getShopNum() + v)
                        .build();
                shopcarCommonService.updateShopCarByPhone(shopcar);
            }
        });
        return true;
    }


    /**
     * 获取本机ip地址
     *
     * @return
     * @throws UnknownHostException
     */
    private String ipAddress() {
        InetAddress IP = null;
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String localIp = IP.getHostAddress();
        return localIp;
    }

    /**
     * 循环遍历购物车，返回购物车信息显示
     *
     * @param shopcarList
     * @return
     */
    private List ergodic(List<Shopcar> shopcarList) {
        List<ShopcarInfoParam> infoParamList = Lists.newArrayList();
        // 循环遍历
        shopcarList.forEach(shopcar -> {
            try {
                // 查询要返回的信息
                GoodsParamDetails goodsParamDetails = productsConsumerService.ProductsDetails(shopcar.getGoodId());
                ShopcarInfoParam shopcarInfoParam = ShopcarInfoParam.builder()
                        .shopNum(shopcar.getShopNum())
                        .discountedPrice(goodsParamDetails.getSellMoney())
                        .build();
                BeanUtils.copyProperties(goodsParamDetails, shopcarInfoParam);
                shopcarInfoParam.setTotalPrice(shopcarInfoParam.getDiscountedPrice() * shopcar.getShopNum());
                infoParamList.add(shopcarInfoParam);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return infoParamList;
    }

    /**
     * 判断数据库购物车有没有相同goodId
     *
     * @param phone
     * @param goodId
     * @param shopNum
     * @return
     */
    private String mysqlJudge(String phone, String goodId, Integer shopNum) {

        // 判断将数据库是否已存在此商品，存在数量叠加
        Shopcar mysqlShopcar = shopcarCommonService.queryShopCarByGoodId(phone, goodId);
        if (null != mysqlShopcar) {
            mysqlShopcar.setShopNum(shopNum + mysqlShopcar.getShopNum());
            shopcarCommonService.updateShopCarByPhone(mysqlShopcar);
            return "0";
        } else {
            Shopcar shopcar = Shopcar.builder()
                    .phone(phone)
                    .goodId(goodId)
                    .shopNum(shopNum)
                    .build();
            shopcarCommonService.insertByPhone(shopcar);
            return "0";
        }

    }

}
