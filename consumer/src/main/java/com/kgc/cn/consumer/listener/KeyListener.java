package com.kgc.cn.consumer.listener;

import com.kgc.cn.consumer.utils.Send.SendSms;
import com.kgc.cn.consumer.utils.active.ActiveMQUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.jms.annotation.JmsListener;

/**
 * Created by Ding on 2019/12/24.
 */
public class KeyListener extends KeyExpirationEventMessageListener {
    @Autowired
    private ActiveMQUtils activeMQUtils;

    public KeyListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String key = message.toString();
        if (key.contains("member")) {
            activeMQUtils.sendMsgByQueue("expireMember", key.substring(7));
        }
    }

    /**
     * 监听过期key，发送会员到期短信
     *
     * @param phone
     */
    @JmsListener(destination = "expireMember")
    public void sendMsg(String phone) {
        SendSms.sendSms(phone, "999999999");
    }

}
