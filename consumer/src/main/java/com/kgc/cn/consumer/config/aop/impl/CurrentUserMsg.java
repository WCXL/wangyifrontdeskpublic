package com.kgc.cn.consumer.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


public class CurrentUserMsg implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        //判断所需参数是不是Object并且有没有@CurrentUser注解
        return parameter.getParameterType().isAssignableFrom(Object.class) &&
                parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String userJsonStr = webRequest.getAttribute("userJsonStr", RequestAttributes.SCOPE_REQUEST).toString();
        Object object = null;
        if (userJsonStr.contains("openid")) {
            object = JSONObject.parseObject(userJsonStr, Weixinuser.class);
        } else {
            object = JSONObject.parseObject(userJsonStr, User.class);
        }
        if (null != object) {
            return object;
        }
        return null;
    }
}
