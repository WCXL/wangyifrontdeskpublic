package com.kgc.cn.consumer.service.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.service.OrderCommonService;
import com.kgc.cn.common.service.PayCommonService;
import com.kgc.cn.consumer.service.PayConsumerService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.wx.QRCodeUtil;
import com.kgc.cn.consumer.utils.wxPay.WxPay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
@Service
public class PayConsumerServiceImpl implements PayConsumerService {
    @Reference
    private PayCommonService payCommonService;

    @Reference
    private OrderCommonService orderCommonService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private ProductsConsumerImpl productsConsumer;

    /**
     * 生成未付款订单
     *
     * @param phone
     * @param shippingId
     * @param goodIdSet
     * @return
     * @throws Exception
     */
    @Override
    public List<Goodorder> creatNoPayOrder(String phone, int shippingId, Set<String> goodIdSet) throws Exception {
        String fenzu = "shopping:";
        goodIdSet.forEach(goodId -> {
            redisUtils.set(fenzu + phone, goodId + ":" + productsConsumer.ProductsNum(goodId), 15);
        });
        return orderCommonService.noPayOrder(phone, shippingId, goodIdSet);
    }

    /**
     * 获得生成支付二维码的字符串
     *
     * @param list
     * @return
     */

    @Autowired
    private WxPay wxPay;

    public String WxPayCode(List<Goodorder> list) throws Exception {
        return wxPay.weixinUrl(list);
    }

    /**
     * code转二维码
     *
     * @param code
     * @return
     */

    @Override
    public String codeToPicture(String code) throws Exception {
        // 生成二维码
        QRCodeUtil.encode(code, "../wangyifrontdesk/consumer/src/main/resources/static/code.jpg");
        return "请前往扫码";
    }

    /**
     * 通过订单号查找商品
     *
     * @param orderId
     * @return
     */

    public List<Goodorder> selectgoodByOrderId(String orderId) {
        return orderCommonService.selectgoodByOrderId(orderId);


    }

    /**
     * 通过订单修改订单状态
     *
     * @param orderId
     * @return
     */
    @Override
    public int updateToOrdered(String orderId) {
        return orderCommonService.updateToOrdered(orderId);
    }


    /**
     * 更新库存
     *
     * @param orderId
     * @return
     */

    @Override
    public int updateNum(String orderId) {
        return orderCommonService.updateNum(orderId);
    }

}
