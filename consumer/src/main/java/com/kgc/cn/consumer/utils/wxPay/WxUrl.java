package com.kgc.cn.consumer.utils.wxPay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 用于获取配置的参数
 *
 * @auther zhouxinyu
 * @data 2019/12/10
 */
@ConfigurationProperties(prefix = "wxPay")
@Component
@Data
public class WxUrl {
    private String appid;
    private String mchId;
    private String key;
    private String notifyUrl;
    private String postUrl;
}
