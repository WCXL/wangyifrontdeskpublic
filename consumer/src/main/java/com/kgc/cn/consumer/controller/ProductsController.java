package com.kgc.cn.consumer.controller;

import com.kgc.cn.common.enums.GoodsEnum;
import com.kgc.cn.common.returnResult.ReturnResult;
import com.kgc.cn.common.returnResult.ReturnResultUtils;
import com.kgc.cn.consumer.paramModel.GoodsParam;
import com.kgc.cn.consumer.paramModel.PageBeanParam;
import com.kgc.cn.consumer.service.ProductsConsumerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "商品")
@RequestMapping(value = "/products")
public class ProductsController {
    @Autowired
    private ProductsConsumerService productsConsumer;

    /**
     * 商品列表要按类型来展示商品列表（从后台中获取商品列表）
     */
    @ApiOperation(value = "按类型来展示商品列表")
    @PostMapping(value = "/ProductList")
    public ReturnResult ProductList(@ApiParam("类型id") @RequestParam int typeId,
                                    @ApiParam("当前页") @RequestParam(defaultValue = "1") String current,
                                    @ApiParam("每页数量") @RequestParam(defaultValue = "5") String size) throws Exception {
        //转成int型
        int sizeInt = Integer.parseInt(size);
        int currentInt = Integer.parseInt(current);
        PageBeanParam<GoodsParam> pageBeanParam = productsConsumer.ProductList(typeId, currentInt, sizeInt);
        if (null == pageBeanParam) {
            return ReturnResultUtils.returnFail(GoodsEnum.VALUE_ERROR);
        }
        return ReturnResultUtils.returnSuccess(pageBeanParam);
    }

    /**
     * 商品详情（要体现 名字 原价 实付价格 图片 描述）
     */
    @ApiOperation(value = "商品详情")
    @PostMapping(value = "/ProductsDetails")
    public ReturnResult ProductsDetails(@ApiParam("商品id") @RequestParam String goodId) throws Exception {

        if (null == productsConsumer.ProductsDetails(goodId)) {
            return ReturnResultUtils.returnFail(GoodsEnum.GOODS_EMPTY);
        }
        return ReturnResultUtils.returnSuccess(productsConsumer.ProductsDetails(goodId));
    }
}
