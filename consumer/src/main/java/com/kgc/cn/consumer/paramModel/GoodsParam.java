package com.kgc.cn.consumer.paramModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@ApiModel("按类型来展示商品列表model")
@AllArgsConstructor
@NoArgsConstructor
public class GoodsParam implements Serializable {
    private static final long serialVersionUID = -516709264450318028L;
    @ApiModelProperty(value = "商品名称")
    private String goodName;
    @ApiModelProperty(value = "商品价格", example = "1")
    private Integer goodPrice;
    @ApiModelProperty(value = "图片路径")
    private String pictureSource;

}
