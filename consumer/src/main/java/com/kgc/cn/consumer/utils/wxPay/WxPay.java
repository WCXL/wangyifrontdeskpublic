package com.kgc.cn.consumer.utils.wxPay;

import com.google.common.collect.Maps;
import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.consumer.service.Impl.ProductsConsumerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * 返回生成微信支付二维码所需要的字符串
 *
 * @auther zhouxinyu
 * @data 2019/12/10
 */
@Component
public class WxPay {
    @Autowired
    WxUrl wxUrl;
    @Autowired
    private ProductsConsumerImpl productsConsumer;

    public String weixinUrl(List<Goodorder> list) throws Exception {
        String body = "";
        String orderId = list.get(0).getOrderId();
        int sumPrice = 0;
        for (Goodorder goodorder : list) {
            sumPrice += Double.parseDouble(goodorder.getShopMoney());
            body += productsConsumer.ProductsDetails(goodorder.getGoodId()).getGoodContent()
                    + "/￥" + (double) productsConsumer.ProductsDetails(goodorder.getGoodId()).getGoodPrice() / 100 + ",";
        }
        SortedMap<String, String> map = Maps.newTreeMap();
        map.put("appid", wxUrl.getAppid());
        map.put("mch_id", wxUrl.getMchId());
        map.put("nonce_str", WxUtil.generateCode());
        map.put("body", body);
        map.put("out_trade_no", orderId);
        map.put("total_fee", String.valueOf(sumPrice));
        map.put("spbill_create_ip", "192.168.137.1");
        map.put("notify_url", wxUrl.getNotifyUrl());
        map.put("trade_type", "NATIVE");
        String sign = WxUtil.generateSignature(map, wxUrl.getKey());
        map.put("sign", sign);
        String Payxml = WxUtil.mapToXml(map);
        String resultXml = WxUtil.doPost(wxUrl.getPostUrl(), Payxml, 10000);
        Map map1 = WxUtil.xmlToMap(resultXml);
        return map1.get("code_url").toString();
    }
}
