package com.kgc.cn.consumer.controller;

import com.kgc.cn.common.enums.ShopcarEnum;
import com.kgc.cn.common.returnResult.ReturnResult;
import com.kgc.cn.common.returnResult.ReturnResultUtils;
import com.kgc.cn.consumer.paramModel.PageBeanParam;
import com.kgc.cn.consumer.paramModel.ShopcarInfoParam;
import com.kgc.cn.consumer.service.ShopcarConsumerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by boot on 2019/12/24
 */
@Api(tags = "购物车")
@RestController
@RequestMapping(value = "/shopcar")
public class ShopcarController {

    @Autowired
    private ShopcarConsumerService consumerService;

    /**
     * 添加购物车
     *
     * @param goodId
     * @param shopNum
     * @return
     * @throws UnknownHostException
     */
    @ApiOperation(value = "添加购物车")
    @PostMapping(value = "/add")
    public ReturnResult addShopcar(@ApiParam(value = "添加到购物车的商品id") @RequestParam String goodId,
                                   @ApiParam(value = "添加到购物车的商品数量") @RequestParam(defaultValue = "1") int shopNum,
                                   HttpServletRequest request) throws UnknownHostException {
        String flag = consumerService.addShopcar(goodId, shopNum, request);

        if (flag.equals("200")) {
            return ReturnResultUtils.returnFail(ShopcarEnum.PHONENUM_ERROR);
        }
        if (flag.equals("201")) {
            return ReturnResultUtils.returnFail(ShopcarEnum.SHOPNUM_ERROR);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 填了商品id则删除对应购物车信息
     * 未填则清空购物车信息
     *
     * @param goodId
     * @return
     */
    @ApiOperation(value = "删除或清空购物车")
    @PostMapping(value = "/delete")
    public ReturnResult deleteShopcar(@ApiParam(value = "欲删除的商品id") @RequestParam String goodId,
                                      HttpServletRequest request) throws Exception {

        String flag = consumerService.deleteShopcar(goodId, request);
        if (flag.equals("200")) {
            return ReturnResultUtils.returnFail(ShopcarEnum.PHONENUM_ERROR);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 展示购物车列表
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "展示购物车")
    @PostMapping(value = "/showShopcar")
    public ReturnResult showShopcar(@ApiParam("当前页") @RequestParam(defaultValue = "1") String current,
                                    @ApiParam("每页数量") @RequestParam(defaultValue = "5") String size,
                                    HttpServletRequest request) throws Exception {
        //转成int型
        int sizeInt = Integer.parseInt(size);
        int currentInt = Integer.parseInt(current);

        PageBeanParam pageBeanParam = consumerService.showShopcar(currentInt,sizeInt,request);
        List<ShopcarInfoParam> shopCarList = pageBeanParam.getData();
        if (CollectionUtils.isEmpty(shopCarList)){
            return ReturnResultUtils.returnFail(ShopcarEnum.SHOPCAR_NULL_ERROR);
        }
        return ReturnResultUtils.returnSuccess(pageBeanParam);
    }

}
