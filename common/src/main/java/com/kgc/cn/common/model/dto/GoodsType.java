package com.kgc.cn.common.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2019/12/16
 */
@Data
public class GoodsType implements Serializable {
    private static final long serialVersionUID = -443195741399325646L;
    //类型id
    private int typeId;
    //类型名称
    private String typeName;
}
