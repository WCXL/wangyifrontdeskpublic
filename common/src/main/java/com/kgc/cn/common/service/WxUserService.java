package com.kgc.cn.common.service;

import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;

/**
 * Created by Ding on 2019/12/23.
 */
public interface WxUserService {
    //通过openid查询微信用户对象
    Weixinuser queryUserByOpenId(String openId);

    //新增一个微信用户对象
    int insertWeiXinUser(Weixinuser weixinuser);

    //通过手机号查询普通用户对象
    User queryUserByPhone(String phone);

    //通过openid更新微信用户的手机号
    int updateWxUserByOpenid(String openid, String phone);

    //增加一个普通用户
    int addUser(User user);
}
