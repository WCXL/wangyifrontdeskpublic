package com.kgc.cn.common.service;

import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.model.dto.Goods;

import java.util.List;
import java.util.Set;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
public interface OrderCommonService {
    // 生成未付款订单
    List<Goodorder> noPayOrder(String phone, int shippingId, Set<String> goodIdSet) throws Exception;

    // 根据goodid返回商品
    Goods seleceGoodsById(String goodId);

    // 查找订单所有商品
    List<Goodorder> selectgoodByOrderId(String orderId);

    //更新订单状态
    int updateToOrdered(String orderId);

    // 更新库存
    int updateNum(String orderId);

}
