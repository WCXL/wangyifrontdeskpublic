package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Weixinuser implements Serializable {
    private static final long serialVersionUID = -5347739971049716472L;
    //oppenId
    private String openId;
    //登入时间
    private String loginTime;
    //手机号
    private String phone;
    //性别;1：男，2：女
    private Integer sex;
    //昵称
    private String nickName;
    //头像地址
    private String headImgUrl;
}