package com.kgc.cn.common.enums;

/**
 * Created by Ding on 2019/12/24.
 */
public enum MemberEnum {
    PHONE_NULL(201, "您还未绑定手机号，请先绑定手机号!"),
    PAY_ERROR(202, "支付失败!");

    int code;
    String msg;

    MemberEnum() {
    }

    MemberEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
