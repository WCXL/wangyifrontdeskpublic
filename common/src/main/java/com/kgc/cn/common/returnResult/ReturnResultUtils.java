package com.kgc.cn.common.returnResult;

import com.kgc.cn.common.enums.LoginRegisterEnum;
import com.kgc.cn.common.enums.MemberEnum;
import com.kgc.cn.common.enums.PayEnum;
import com.kgc.cn.common.enums.ShopcarEnum;

import com.kgc.cn.common.enums.LoginRegisterEnum;

import com.kgc.cn.common.enums.GoodsEnum;

/***
 *
 * 统一返回工具类
 */
public class ReturnResultUtils {

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage("success");
        return returnResult;
    }

    /***
     * 成功 带信息
     * @return
     */
    public static ReturnResult returnSuccess(String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(message);
        return returnResult;
    }


    /***
     * 成功 带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 成功 带数据 带信息
     * @return
     */
    public static ReturnResult returnSuccess(Object data, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage(message);
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 失败使用枚举（Pay）
     * @return
     */
    public static ReturnResult returnFail(PayEnum payEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(payEnum.getCode());
        returnResult.setMessage(payEnum.getMsg());
        return returnResult;
    }

    /***
     * 失败,使用枚举LoginRegisterEnum
     * @return
     */
    public static ReturnResult returnFail(LoginRegisterEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }

    /**
     * 失败使用枚举（goods）
     */
    public static ReturnResult returnFail(GoodsEnum goodsEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(goodsEnum.getCode());
        returnResult.setMessage(goodsEnum.getMsg());
        return returnResult;
    }

    /***
     * 失败,使用枚举MemberEnum
     * @return
     */
    public static ReturnResult returnFail(MemberEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }

    /***
     * 失败,使用枚举MemberEnum
     * @return
     */
    public static ReturnResult returnFail(ShopcarEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }


}
