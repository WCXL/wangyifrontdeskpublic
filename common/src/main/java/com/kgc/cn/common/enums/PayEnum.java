package com.kgc.cn.common.enums;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
public enum PayEnum {
    PAY_FAIL(101, "付款失败！"),

    PAY_SUCCESS(102, "支付成功！即将跳转至首页..."),

    PAY_NOPHONE(103, "请先去绑定手机号！");

    int code;
    String msg;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    PayEnum() {
    }

    PayEnum(String msg) {
        this.msg = msg;
    }

    PayEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
