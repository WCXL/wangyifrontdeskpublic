package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Shopcar implements Serializable {
    private static final long serialVersionUID = 2979122815275045207L;
    //手机号
    private String phone;
    //商品id
    private String goodId;
    //购买数量
    private Integer shopNum;
}